import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;

public class MissingN {

    public static void main(String[] args) throws IOException {
        System.out.println("Enter the desired array size (>= "
                + MissingN.MIN_ARRAY_SIZE + "): ");
        BufferedReader in = new BufferedReader(
                new InputStreamReader(System.in));
        String input = in.readLine();
        int arraySize;
        try {
            arraySize = Integer.parseInt(input);
        } catch (NumberFormatException ex) {
            System.err.println("Invalid input, integer number expected");
            System.exit(1);
            return;
        }

        if (arraySize < MissingN.MIN_ARRAY_SIZE) {
            System.err.println("Invalid input, out of range");
            System.exit(1);
            return;
        }

        MissingN impl = new MissingN(arraySize);

        System.out.println("Generated array:");
        System.out.println(Arrays.toString(impl.getSourceArray()));
        System.out.println("Modified array:");
        System.out.println(Arrays.toString(impl.getModifiedArray()));
        System.out.println("Missing value:");
        System.out.println(impl.getMissingValue());
    }

    public static final int MIN_ARRAY_SIZE = 10;

    private final int[] sourceArray;
    private final int[] modifiedArray;
    private final int missingValue;

    public MissingN(int generatedArraySize) {
        if (generatedArraySize < MIN_ARRAY_SIZE) {
            throw new IllegalArgumentException("Out of range");
        }
        this.sourceArray = IntArrayUtils.iota(generatedArraySize, 0);
        this.modifiedArray = IntArrayUtils.removeRandom(this.sourceArray);
        IntArrayUtils.shuffle(this.modifiedArray);
        this.missingValue = this.sourceArray[
                IntArrayUtils.findMissing(this.modifiedArray)];
    }

    public int[] getSourceArray() {
        return sourceArray.clone();
    }

    public int[] getModifiedArray() {
        return modifiedArray.clone();
    }

    public int getMissingValue() {
        return missingValue;
    }
}
