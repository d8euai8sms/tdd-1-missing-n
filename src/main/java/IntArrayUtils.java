import java.util.BitSet;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.IntStream;

public class IntArrayUtils {

    // @requires 0 <= length
    public static int[] iota(int length, int initialValue) {
        int[] arr = new int[length];
        for (int i = 0; i < length; ++i) {
            arr[i] = initialValue++;
        }
        return arr;
    }

    // @requires 0 <= index && index <= arr.length
    public static int[] removeAt(int[] arr, int index) {
        int[] res = new int[arr.length - 1];
        System.arraycopy(arr, 0, res, 0, index);
        System.arraycopy(arr, index + 1, res, index, arr.length - 1 - index);
        return res;
    }

    // @requires arr.length != 0
    public static int[] removeRandom(int[] arr) {
        return removeAt(arr,
                ThreadLocalRandom.current().nextInt(arr.length));
    }

    // naive shuffle
    public static void shuffle(int[] arr) {
        ThreadLocalRandom r = ThreadLocalRandom.current();
        // @invariant shuffled(arr[0..i])
        for (int i = 0; i < arr.length - 1; ++i) {
            // include i so that swap is not required to happen
            int idx = r.nextInt(i, arr.length);
            if (i != idx) swap(arr, i, idx);
        }
    }

    private static void swap(int[] arr, int i, int j) {
        int tmp = arr[i];
        arr[i] = arr[j];
        arr[j] = tmp;
    }

    // the most optimized implementation
    //
    // @requires exists v . sort(arr++v) == iota(0,arr.length+1)
    public static int findMissing(int[] arr) {
        // basic idea:
        //     arr1 = 0, 1, ... n-1
        //     sum of arr1 = (0 + (n-1)) / 2 * n
        //     sum of arr2 = sum of arr1 - x, where x is missing element value
        //     x = sum of arr1 - sum of arr2

        long sum = 0;
        for (int v : arr) { sum += v; }

        long an = arr.length;
        // will not overflow as (2^31 - 1) * 2^31 ~= 2^62 < 2^63 ~= max long
        long expectedSum = ((0L + an) * (arr.length + 1)) / 2;

        long missing = expectedSum - sum;

        // value is same as its index
        int missingIndex = (int) missing;

        if (missingIndex < 0 || missingIndex > arr.length) {
            throw new IllegalArgumentException(
                    "Contract violation");
        }

        return missingIndex;
    }
}
