import static org.junit.Assert.*;
import org.junit.Test;
import static org.hamcrest.CoreMatchers.*;

import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class MissingNAcceptanceTest {

    @Test
    public void whenInputIsValid_thenResultIsCorrect() {
        final int validArraySize = 25; // n >= 10

        MissingN sut = new MissingN(validArraySize);

        int[] source = sut.getSourceArray();
        int[] modified = sut.getModifiedArray();
        int missingValue = sut.getMissingValue();

        assertThat(modified.length, is(source.length - 1));
        assertThat(IntStream.of(source).boxed()
                .collect(Collectors.toList()), hasItem(missingValue));
        assertThat(IntStream.of(modified).boxed()
                .collect(Collectors.toList()), not(hasItem(missingValue)));
        assertThat(IntStream.of(source)
                .filter(i -> i != missingValue)
                .sorted()
                .toArray(), is(IntStream.of(modified)
                                    .sorted()
                                    .toArray()));
    }

    @Test(expected = IllegalArgumentException.class)
    public void whenInputIsInvalid_thenIllegalArgumentException() {
        final int invalidArraySize = 2; // n < 10

        new MissingN(invalidArraySize);
    }
}
