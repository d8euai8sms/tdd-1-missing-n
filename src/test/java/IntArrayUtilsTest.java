import static org.junit.Assert.*;
import org.junit.Test;
import static org.hamcrest.CoreMatchers.*;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class IntArrayUtilsTest {

    private static int anyBetween(int a, int b) {
        return ThreadLocalRandom.current().nextInt(a, b);
    }

    private static int[] anyIntArray(int minSize, int maxSize) {
        int limit = anyBetween(minSize, maxSize);
        return ThreadLocalRandom.current().ints(limit)
                .toArray();
    }

    private static int[] anyIntArray() {
        return anyIntArray(5, 15);
    }

    private static int[] anyDistinctIntArray(int minSize, int maxSize) {
        int limit = anyBetween(minSize, maxSize);
        return ThreadLocalRandom.current().ints()
                .distinct()
                .limit(limit)
                .toArray();
    }

    private static int[] anyDistinctIntArray() {
        return anyDistinctIntArray(5, 15);
    }

    private static int anyIndexIn(int[] arr) {
        return anyBetween(0, arr.length);
    }

    private static int[] anyIota(int minSize, int maxSize) {
        int limit = anyBetween(minSize, maxSize);
        return IntStream.range(0,limit).toArray();
    }

    private static int[] anyIota() {
        return anyIota(5, 15);
    }

    @Test
    public void iota_whenLengthIsValid_thenReturnsArrayOfProperLengthFilledWithProgressiveInts() {
        final int length = 25;
        final int initial = 78;

        assertThat(
                IntArrayUtils.iota(length, initial),
                is(IntStream.range(initial, initial + length).toArray()));
    }

    @Test
    public void removeAt_whenIndexIsValid_thenReturnsNewArrayWithoutThatElement() {
        final int[] arr = anyIntArray();
        final int index = anyIndexIn(arr);

        IntStream firstHalf = IntStream.of(arr).limit(index);
        IntStream secondHalf = IntStream.of(arr).skip(index + 1);

        assertThat(
                IntArrayUtils.removeAt(arr, index),
                is(IntStream.concat(firstHalf, secondHalf).toArray()));
    }

    @Test
    public void shuffle_whenArrayIsGiven_thenReturnsNewArrayWithElementsShuffled() {
        final int[] arr = anyDistinctIntArray(500, 1000);
        final int[] shuffled = arr.clone();

        IntArrayUtils.shuffle(shuffled);

        // shuffle is non-deterministic, so the following
        // assertion may fire in some rare cases:
        assertThat(shuffled, is(not(arr)));
        // but since array size is quite large, the possibility
        // of false positive is relatively small;

        assertThat(
                IntStream.of(shuffled).sorted().toArray(),
                is(IntStream.of(arr).sorted().toArray()));
    }

    @Test
    public void findMissing_whenValidInputIsGiven_thenReturnsCorrectIndexOfMissingValue() {
        final int[] arr = anyIota(10, 50);
        final int indexOfMissing = anyIndexIn(arr);
        final int[] withOneElementMissing =
                IntArrayUtils.removeAt(arr, indexOfMissing);
        IntArrayUtils.shuffle(withOneElementMissing);

        assertThat(
                IntArrayUtils.findMissing(withOneElementMissing),
                is(indexOfMissing));
    }
}
